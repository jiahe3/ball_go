using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCollider : MonoBehaviour
{
    [SerializeField]Score Score;
    private void Start()
    {
        Score = GameObject.FindGameObjectWithTag("Score").GetComponent<Score>();
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
       
        if (collision.gameObject.name == "Circle")
        {
            Score.AddScore(1);
        }
    }
}
