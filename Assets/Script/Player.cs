using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    
    Rigidbody2D RB;
    [SerializeField] float Speed = 5;
    Score Score;
    bool alive = true;

    void Start()
    {
        alive = true;
        Score = GameObject.FindGameObjectWithTag("Score").GetComponent<Score>();
        RB = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Space) && alive )
        {
            RB.velocity = Time.deltaTime*Vector2.up*Speed;
        }
        if(transform.position.y >5 || transform.position.y < -5)
        {
            Score.EndUiappear();
            Destroy(gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        alive = false;
        Score.EndUiappear();
    }
}
