using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Score : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI SC;
    [SerializeField] TextMeshProUGUI MSC;
    int MostScore = 0;
    int NewScore;
    int PlayerScore;
    [SerializeField] GameObject EndUI;
    private void Start()
    {
        MostScore = PlayerPrefs.GetInt("Score");
        EndUI.SetActive(false);
        NewScore = MostScore;
        MSC.text = "Most Score     " + MostScore.ToString();
    }
    public void AddScore(int Add)
    {
        
        PlayerScore = Add+PlayerScore;
        SC.text = PlayerScore.ToString();
    }
    public void EndGame()
    {
        SceneManager.LoadScene("Game");

    }
    public void EndUiappear()
    {
        if (NewScore < PlayerScore)
        {
            NewScore = PlayerScore;
        }
        PlayerPrefs.SetInt("Score", NewScore);
        EndUI.SetActive(true);
    }
    public void Back()
    {
        SceneManager.LoadScene("Title");
    }
   
}
