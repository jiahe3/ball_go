using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwnperPosition : MonoBehaviour
{
    [SerializeField] GameObject SO;
    [SerializeField] float Speed =5;
    float hight = 5;
    float timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        create();
    }

    // Update is called once per frame
    void Update()
    {
        if (timer <= Speed)
        { 
            timer = timer+ Time.deltaTime; 
        }
        else
        {
            timer = 0;
            create();
        }
                
        
    }
    void create()
    {
        float low = transform.position.y - hight;
        float top = transform.position.y + hight;
        Instantiate(SO, new Vector3(transform.position.x,Random.Range(low,top),0), transform.rotation);
    }
}
